# miniProject2

## Find Prime Numbers

###
In this Lambda function, you can find prime numbers in range. For instance, if `range = 10`, then we will find all prime numbers less than 10.


### Screenshots
![Pic1](process-data/screenshots/1.png)

![API Gateway](process-data/screenshots/2.png)

![Pic3](process-data/screenshots/3.png)

![Pic4](process-data/screenshots/4.png)

#### Several ways to run with command:
- `make watch` then `make invoke`

- `cargo lambda invoke --data-ascii '{ "range": 100 }'`

- `aws stepfunctions start-execution --state-machine-arn "arn:aws:states:region:381492062366:stateMachine:MyStateMachine-chpknh43f" --input '{"range": 30}'`
