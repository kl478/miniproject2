use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    range: i32,
}

/// Response structure that will be returned by our Lambda function.
/// The runtime automatically serializes this response into JSON format.
/// The structure contains a vector of prime numbers calculated based on the input range.
#[derive(Serialize)]
struct Response {
    primes: Vec<i32>,
}

/// Determines if a given number `n` is prime.
/// A prime number is a natural number greater than 1 that is not a product of 
/// two smaller natural numbers.
fn is_prime(n: i32) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..((n as f64).sqrt() as i32 + 1) {
        if n % i == 0 {
            return false;
        }
    }

    true
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let range = event.payload.range;

    // Calculate prime numbers within the specified range
    let primes: Vec<i32> = (2..=range).filter(|&n| is_prime(n)).collect();

    // Prepare the response
    let resp = Response {
        primes,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
